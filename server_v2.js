//imports y arranque de framework express en puerto
var express = require('express');
//app framework variable
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

app.use(bodyParser.json());

var baseMLabURL =  "https://api.mlab.com/api/1/databases/apitechunol/collections/";
var mLabAPIKey = "apiKey=cUz5TOj1kVHCy8vnDlIs46nphaBBJDC7";
var requestJSON = require('request-json');

var faker = require('faker');
var jwtTokenKey = "1234567890";

// listen at defined port
app.listen(port);
console.log("API molona Escuchando en el puerto " + port);

app.get("/apitechu/v3",
// http request y response, los monta el  framework
  function(req, res) {
    console.log("GET /apitechu/v3");
    // Si se pone sin comilla simple el retorno lo interpreta directamente como un json
    //res.send('{"msg" : "Hola desde el puerto 3000 de APITechU"}');
    res.send({"msg" : "Hola desde el puerto 3000 de APITechU - v3"});
  }

);

app.get ("/apitechu/v3/users",
  function(req, res) {
    console.log("GET /apitechu/v3/users");
    commonManageJWTHeaderCallback(req,res,
      function(req, res) {

        var httpClient = requestJSON.createClient(baseMLabURL);
        console.log("Cliente HTTP lanzado");

        httpClient.get("user?" + mLabAPIKey,
          function(err, resMLab, body) {
            var response = !err ? body : {
              "msg" : "Error obteniendo usuarios"
            }
            res. send(response);
          }
        )
      }
    )
  }
);

app.get ("/apitechu/v3/users/:id",
  function(req, res) {
    console.log("GET /apitechu/v3/users/:id");
    commonManageJWTHeaderCallback(req,res,
      function(req, res) {
        var id = req.params.id;
        var query = 'q={"id":' + id + '}';

        var httpClient = requestJSON.createClient(baseMLabURL);
        //console.log("Cliente HTTP lanzado");
        //console.log("user?" + query + "&"+ mLabAPIKey);
        httpClient.get("user?" + query + "&"+ mLabAPIKey,
          function(err, resMLab, body) {

            var response = {};
            if (err) {
              console.log("error = " + err);
              response = {
                "msg" : "Error obteniendo usuario"
              }
              res.status(500);
              console.log("Error obteniendo usuario. code("+ err +")");
            } else {
              if (body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario no encontrado"
                }
                res.status(404);
              }
            }

            res. send(response);
          }
        )
      }
    )
  }
);

app.get ("/apitechu/v2/users/:id",
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id");
    commonManageJWTHeader(req, res);
    var id = req.params.id;
    var query = 'q={"id":' + id + '}';

    var httpClient = requestJSON.createClient(baseMLabURL);
    //console.log("Cliente HTTP lanzado");
    //console.log("user?" + query + "&"+ mLabAPIKey);
    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(err, resMLab, body) {

        var response = {};
        if (err) {
          console.log("error = " + err);
          response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
          console.log("Error obteniendo usuario. code("+ err +")");
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }

        res. send(response);
      }
    )
  }
);

/***
 * Deletes the user with the given PK _id.
 * Retrieves the user accounts and those account movements and pseudo deletes them as MLab
 * api does not allow multiple delete in a single operation (leaves, empty fields with _id intact).
 * In theory we would have to go _id by _id deleting every document (not performed for TechU app).
 ***/
 /**
 app.delete ("/apitechu/v2/users/:_id",
   function(req, res) {
     console.log("DELTE /apitechu/v2/users//:_id");

     var objectId = req.params._id;
     console.log(req.params);
     console.log(req.params._id);

     var httpClient = requestJSON.createClient(baseMLabURL);
     console.log("Cliente HTTP lanzado");
     console.log("user/" + objectId + "?"+ mLabAPIKey);
     httpClient.delete("user/" + objectId + "?"+ mLabAPIKey,
       function(err, resMLab, bodyDelete) {

         var response = {};
         console.log("error = " + err);
         console.log("body = " + bodyDelete);
         console.log(JSON.stringify(bodyDelete));
         if (err) {
           response = {
             "msg" : "Error deleting user"
           }
           res.status(500);
           console.log("Error deleting user. code("+ err +")");
         } else {
           response = bodyDelete;
         }

         res. send(response);
       }
     )
   }
 );
**/

/***
 * Deletes the user with the given id. (not mongoDB PK)
 * Retrieves the user accounts and those account movements and pseudo deletes them as MLab
 * api does not allow multiple delete in a single operation (leaves, empty fields with _id intact).
 * In theory we would have to go _id by _id deleting every document (not performed for TechU app).
 ***/

 app.delete ("/apitechu/v2/users/:id",
   function(req, res) {
     console.log("DELTE /apitechu/v2/users/:id");
     commonManageJWTHeader(req, res);

     var id = req.params.id;
     var query = 'q={"id":' + id + '}';

     var putBody = {};

     console.log(req.params);
     console.log(req.params.id);

     var httpClient = requestJSON.createClient(baseMLabURL);
     console.log("Cliente HTTP lanzado");
     httpClient.put("user?" + query + "&"+ mLabAPIKey, putBody,
       function(errPutReturn, resMLab, bodyPutReturn) {

         var response = {};
         console.log("errorPutReturn = " + errPutReturn);
         console.log(JSON.stringify(bodyPutReturn));
         if (errPutReturn) {
           response = {
             "msg" : "Error deleting user"
           }
           res.status(500);
           console.log("Error deleting user. code("+ errPutReturn +")");
         } else {
           response = bodyPutReturn;
         }
         // Delete all accounts and delete all movements


         res. send(response);
       }
     )
   }
 );

//Add a new user to the DB.
app.post ("/apitechu/v2/users",
  function(req, res) {
    console.log("POST /apitechu/v2/users");
    commonManageJWTHeader(req, res);

    console.log(req.body);

    var getQuery = 's={"id" : -1} ';

    var postBody = req.body;

    console.log("No Cliente HTTP lanzado");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("user?" + getQuery + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error en la creación de nuevo usuario"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {


          if (bodyGet.length > 0) {
            console.log("bodyGet.length > 0");
            console.log(bodyGet[0].id);
            var newUserId = bodyGet[0].id + 1;
            postBody.id = newUserId;
            console.log(postBody);
            //console.log(JSON.parse(postBody));
            console.log("user?" + mLabAPIKey + "_______" + postBody);
            httpClient.post("user?" + mLabAPIKey, postBody,
              function(errPost, resMLabPost, bodyPost) {
                if (errPost) {
                  response = {
                    "msg" : "Error durante la creación de nuevo usuario"
                  }
                  res.status(500);
                  console.log("Error durante la creación de nuevo usuario("+ errPost +")");
                } else {
                  console.log("Usuario creado correctamente");
                  response = {
                    "msg" : "Creación correcta del usuario",
                    "userid" : newUserId
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Eror en la creación de nuevo usuario"
            }
            console.log("Eror en la creación de nuevo usuario");
            res.status(404);
            res.send(response);
          }
        }
      }
    )

  }
);

//Update a given user to the DB.
app.put ("/apitechu/v2/users/:id",
  function(req, res) {
    console.log("PUT /apitechu/v2/users/:id");
    if (commonManageJWTHeader(req, res)) {
      console.log(req.body);
      var updateUserId = req.params.id;

      var getQuery = 'q={"id" : ' + updateUserId + '} ';

      var putBody = req.body;
      putBody.id = Number(updateUserId);

      var httpClient = requestJSON.createClient(baseMLabURL);
      console.log("Cliente HTTP lanzado");
      console.log("user?" + getQuery + "&"+ mLabAPIKey);
      httpClient.put("user?" + getQuery + "&"+ mLabAPIKey, putBody,
        function(errPut, resMLabPut, bodyPut) {
          if (errPut) {
            response = {
              "msg" : "Error durante la actualización de un usuario"
            }
            res.status(500);
            console.log("Error durante la actualización de un usuario("+ errPut +")");
          } else {
            console.log("Usuario actualizado correctamente");
            response = {
              "msg" : "Actualización correcta del usuario",
              "userData" : putBody
            }
          }
          res.send(response);
        }
      )
    }
  }
);

app.post("/apitechu/v3/login",
  function(req, res) {
    console.log("POST /apitechu/v3/login");

    var email = req.body.email;
    var password = req.body.password;

    // simple security token with 1h expiration.
    var token = jwt.sign({ email: req.body.email, password: req.body.password, expire: Math.floor(Date.now() / 1000) + (60 * 60)}, jwtTokenKey);
    console.log(token);

    var query = 'q={"email":"' + email + '", "password":"' + password + '"}';

    var putBody = '{"$set" : {"logged" : "true"}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {


          if (bodyGet.length > 0) {

            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGIN"
                  }
                  res.status(500);
                  console.log("Error durante el LOGIN("+ errPut +")");
                } else {
                  console.log("Log ok");
                  response = {
                    "msg" : "CORRECT LOGIN for user",
                    "userid" : bodyGet[0]["id"],
                    "token"  : token
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Usuario o contraseña incorrectos"
            }
            console.log("Failed LOGIN");
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)

app.post("/apitechu/v2/login",
  function(req, res) {
    console.log("POST /apitechu/v2/login");

    var email = req.body.email;
    var password = req.body.password;

    console.log(req.body);

    var query = 'q={"email":"' + email + '", "password":"' + password + '"}';

    var putBody = '{"$set" : {"logged" : "true"}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {


          if (bodyGet.length > 0) {

            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGIN"
                  }
                  res.status(500);
                  console.log("Error durante el LOGIN("+ errPut +")");
                } else {
                  console.log("Log ok");
                  response = {
                    "msg" : "CORRECT LOGIN for user",
                    "userid" : bodyGet[0]["id"]
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Usuario o contraseña incorrectos"
            }
            console.log("Failed LOGIN");
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)

app.post("/apitechu/v2/logout",
  function(req, res) {
    console.log("POST /apitechu/v2/logout");
    //commonManageJWTHeader(req, res);

    var id = req.body.id;

    var query = 'q={"id":' + id + '}';

    var putBody = '{"$unset" : {"logged" : ""}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {

          if (bodyGet.length > 0) {
            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGOUT"
                  }
                  res.status(500);
                  console.log("Error durante el LOGOUT("+ errPut +")");
                } else {
                  console.log("LOGOUT ok");
                  response = {
                    "msg" : "CORRECT LOGOUT for user"
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Logger user not found"
            }
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)

////
// ACCOUNTS
////

app.get ("/apitechu/v2/users/:id/accounts",
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/accounts");
    commonManageJWTHeader(req, res);

    var id = req.params.id;
    var query = 'q={"userid":' + id + '}';

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");
    httpClient.get("account?" + query + "&"+ mLabAPIKey,
      function(err, resMLab, body) {

        var response = {};
        console.log("error = " + err);
        if (err) {
          response = {
            "msg" : "Error obteniendo account"
          }
          res.status(500);
          console.log("Error obteniendo account. code("+ err +")");
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "El usuario no tiene cuentas"
            }
            res.status(404);
          }
        }

        res. send(response);
      }
    )
  }
);

/***
 ** Delete an account for the given objectId _id
***/
app.delete ("/apitechu/v2/accounts/:_id",
  function(req, res) {
    console.log("DELTE /apitechu/v2/accounts/:_id");
    commonManageJWTHeader(req, res);

    var objectId = req.params._id;

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");
    httpClient.delete("account/" + objectId + "?"+ mLabAPIKey,
      function(err, resMLab, bodyDelete) {

        var response = {};
        console.log("error = " + err);
        if (err) {
          response = {
            "msg" : "Error deleting account"
          }
          res.status(500);
          console.log("Error deleting account. code("+ err +")");
        } else {
          response = bodyDelete;
        }

        res. send(response);
      }
    )
  }
);


//Add a new account to the DB.
app.post ("/apitechu/v2/accounts",
  function(req, res) {
    console.log("POST /apitechu/v2/accounts");
    commonManageJWTHeader(req, res);

    console.log(req.body);

    var userid = req.body.userid;
    var postBody = {"userid" : req.body.userid};


    var getQuery = 's={"id" : -1} ';

    console.log("No Cliente HTTP lanzado");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("account?" + getQuery + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error en la creación de nueva cuenta"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo cuenta. code("+ errGet +")");
        } else {

          if (bodyGet.length > 0) {
            // Create new account inforation
            var newAccountId =
            postBody.id = bodyGet[0].id + 1; //new account id
            postBody.IBAN = faker.finance.iban();
            postBody.balance = 0;

            console.log(postBody);
            console.log("account?" + mLabAPIKey + "_______" + postBody);
            httpClient.post("account?" + mLabAPIKey, postBody,
              function(errPost, resMLabPost, bodyPost) {
                if (errPost) {
                  response = {
                    "msg" : "Error durante la creación de nueva cuenta"
                  }
                  res.status(500);
                  console.log("Error durante la creación de nueva cuenta("+ errPost +")");
                } else {
                  console.log("Cuenta creada correctamente");
                  response = {
                    "msg" : "Creación correcta de la cuenta",
                    "account" : bodyPost
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Eror en la creación de nueva cuenta"
            }
            console.log("Eror en la creación de nueva cuenta");
            res.status(404);
            res.send(response);
          }
        }
      }
    )

  }
);

/****
** Get all movements for a given account id
*****/
app.get ("/apitechu/v2/accounts/:id/movements",
  function(req, res) {
    console.log("GET apitechu/v2/accounts/:id/movements");
    commonManageJWTHeader(req, res);

    var id = req.params.id;
    var query = 'q={"accountid":' + id + '}';

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");
    //console.log("account_movements?" + query + "&"+ mLabAPIKey);
    httpClient.get("account_movements?" + query + "&"+ mLabAPIKey,
      function(err, resMLab, body) {

        var response = {};
        console.log("error = " + err);
        if (err) {
          response = {
            "msg" : "Error obteniendo account movements"
          }
          res.status(500);
          console.log("Error obteniendo account movements. code("+ err +")");
        } else {
          if (body.length > 0) {

            response = body;
          } else {
            response = {
              "msg" : "La cuenta no tiene movimientos"
            }
            res.status(404);
          }
        }

        res. send(response);
      }
    )
  }
);

// movements

//Add a new movement to the DB.
app.post ("/apitechu/v2/movements",
  function(req, res) {
    console.log("POST /apitechu/v2/movements");
    commonManageJWTHeader(req, res);

    console.log(req.body);

    var getQuery = 's={"id" : -1} ';

    var postBody = req.body;

    console.log("No Cliente HTTP lanzado");
    var httpClient = requestJSON.createClient(baseMLabURL);

    console.log(postBody);
    //console.log(JSON.parse(postBody));
    console.log("account_movements?" + mLabAPIKey + "_______" + postBody);
    httpClient.post("account_movements?" + mLabAPIKey, postBody,
      function(errPost, resMLabPost, bodyPost) {
        if (errPost) {
          response = {
            "msg" : "Error durante la creación de nuevo movimiento"
          }
          res.status(500);
          console.log("Error durante la creación de nuevo movimiento("+ errPost +")");
        } else {
          console.log("movimiento creado correctamente");
          response = {
            "msg" : "Creación correcta del movimiento",
            "movement" : bodyPost
          }
        }
        res.send(response);
      }
    )
  }
);

/***
 ** Delete movement for given object id  _id.
***/
app.delete ("/apitechu/v2/movements/:_id",
  function(req, res) {
    console.log("DELTE /apitechu/v2/movements/:_id");
    commonManageJWTHeader(req, res);

    var objectId = req.params._id;
    console.log(req.params);
    console.log(req.params._id);

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");
    console.log("account_movements/" + objectId + "?"+ mLabAPIKey);
    httpClient.delete("account_movements/" + objectId + "?"+ mLabAPIKey,
      function(err, resMLab, bodyDelete) {

        var response = {};
        console.log("error = " + err);
        console.log("body = " + bodyDelete);
        console.log(JSON.stringify(bodyDelete));
        if (err) {
          response = {
            "msg" : "Error deleting movement"
          }
          res.status(500);
          console.log("Error deleting movement. code("+ err +")");
        } else {
          response = bodyDelete;
        }

        res. send(response);
      }
    )
  }
);

function commonManageJWTHeader(req, res) {

  var token = req.headers.noltoken;

  console.log("server header read token");
  console.log(token);

  if (!token)
    return res.status(401).send({ auth: false, message: 'No token provided.' });

  // verify a token symmetric - synchronous
  var decoded = jwt.verify(token, jwtTokenKey, function(err, decoded) {
    // err
    if (err != null) {
      // errror in token verification
      console.log("error in token verification");
      console.log(JSON.stringify(err));
      var errorResponse = {
        "msg" : "Secutiry token validation falied, try to login again.",
        "err" : err,
      }
      res.status(401);
      res.send(errorResponse);
    }
    else {

      // token verified corectly
      console.log(JSON.stringify(decoded));
      return true;
    }
  });
}

function commonManageJWTHeaderCallback(req, res, callback) {

  var token = req.headers.noltoken;

  console.log("server header read token");
  console.log(token);

  if (!token)
    return res.status(401).send({ auth: false, message: 'No token provided.' });

  // verify a token symmetric - synchronous
  var decoded = jwt.verify(token, jwtTokenKey, function(err, decoded) {
    // err
    if (err != null) {
      // errror in token verification
      console.log("error in token verification");
      console.log(JSON.stringify(err));
      var errorResponse = {
        "msg" : "Secutiry token validation falied, try to login again.",
        "err" : err,
      }
      res.status(401);
      res.send(errorResponse);
    }
    else {

      // token verified corectly
      console.log(JSON.stringify(decoded));
      callback(res, req);
    }
  });
}
