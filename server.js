//imports y arranque de framework express en puerto
var express = require('express');
//app framework variable
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

app.use(bodyParser.json());

var baseMLabURL =  "https://api.mlab.com/api/1/databases/apitechunol/collections/";
var baseApiTechuURL = "http://localhost:3000/apitechu/v3/"
var mLabAPIKey = "apiKey=cUz5TOj1kVHCy8vnDlIs46nphaBBJDC7";
var requestJSON = require('request-json');

var faker = require('faker');
var jwtTokenKey = "1234567890"; //low level security :-).

// listen at defined port
app.listen(port);
console.log("API TECHU NOL BANKING escuchando en el puerto " + port);

/********************
*** Health check method
***
*********************/
app.get("/apitechu/v3",
// http request y response, los monta el  framework
  function(req, res) {
    console.log("GET /apitechu/v3");
    // Si se pone sin comilla simple el retorno lo interpreta directamente como un json
    //res.send('{"msg" : "Hola desde el puerto 3000 de APITechU"}');
    res.send({"msg" : "Hola desde el puerto 3000 de APITechU - v3"});
  }

);

/********************
*** Get all users.
*** Returns an array of all valid users
*********************/
app.get ("/apitechu/v3/users",
  function(req, res) {
    console.log("GET /apitechu/v3/users");
    commonManageJWTHeaderCallback(req,res,
      function(req, res) {
        var httpClient = requestJSON.createClient(baseMLabURL);

        httpClient.get("user?" + mLabAPIKey,
          function(err, resMLab, body) {
            var response = !err ? body : {
              "msg" : "Error obteniendo usuarios"
            }
            res. send(response);
          }
        )
      }
    )
  }
);

/********************
*** Get user for given id.
*** Returns the user object for the given id (String Id not primary ObjectId)
*********************/
app.get ("/apitechu/v3/users/:id",
  function(req, res) {
    console.log("GET /apitechu/v3/users/:id");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {
        var id = req.params.id;
        var query = 'q={"id":' + id + '}';

        var httpClient = requestJSON.createClient(baseMLabURL);

        //console.log("user?" + query + "&"+ mLabAPIKey);
        httpClient.get("user?" + query + "&"+ mLabAPIKey,
          function(err, resMLab, body) {

            var response = {};
            if (err) {
              response = {
                "msg" : "Error obteniendo usuario"
              }
              res.status(500);
              console.log("Error obteniendo usuario. code("+ err +")");
            } else {
              if (body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario no encontrado"
                }
                res.status(404);
              }
            }

            res. send(response);
          }
        )
      }
    )
  }
);

/***
 * Deletes the user with the given id. (not mongoDB PK)
 *
 * WARNING: NOT IMPLMENTED AS NOT MAIN GOAL AND MLAB API DEFICIENCIES.
 * Shoudl really retrieve the user accounts and those account movements and pseudo delete them as MLab
 * api does not allow multiple delete in a single operation (leaves, empty fields with _id intact).
 * In theory we would have to go _id by _id deleting every document (not performed for TechU app).
 ***/

 app.delete ("/apitechu/v3/users/:id",
   function(req, res) {
     console.log("DELTE /apitechu/v3/users/:id");
     commonManageJWTHeaderCallback(req, res,
       function(req, res) {

         var id = req.params.id;
         var query = 'q={"id":' + id + '}';

         var putBody = {};

         var httpClient = requestJSON.createClient(baseMLabURL);

         httpClient.put("user?" + query + "&"+ mLabAPIKey, putBody,
           function(errPutReturn, resMLab, bodyPutReturn) {

             var response = {};

             if (errPutReturn) {
               response = {
                 "msg" : "Error deleting user"
               }
               res.status(500);
               console.log("Error deleting user. code("+ errPutReturn +")");
             } else {
               response = bodyPutReturn;
             }
             // Delete all accounts and delete all movements


             res. send(response);
           }
         )
       }
     )
   }
 );

/********************
*** Add a new user to the DB.
*** Adds the user given as part of the JSON payload
*********************/
app.post ("/apitechu/v3/users",
  function(req, res) {
    console.log("POST /apitechu/v3/users");
    // creating a new user is a register of a user, does not require token and LOGIN

    // QUery current users to obtain next valid ID
    var getQuery = 's={"id" : -1} ';

    var postBody = req.body;

    var httpClient = requestJSON.createClient(baseMLabURL);


    httpClient.get("user?" + getQuery + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        if (errGet) {
          response = {
            "msg" : "Error en la creación de nuevo usuario"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {

          if (bodyGet.length > 0) {
            var newUserId = bodyGet[0].id + 1;
            postBody.id = newUserId;

            httpClient.post("user?" + mLabAPIKey, postBody,
              function(errPost, resMLabPost, bodyPost) {
                if (errPost) {
                  response = {
                    "msg" : "Error durante la creación de nuevo usuario"
                  }
                  res.status(500);
                  console.log("Error durante la creación de nuevo usuario("+ errPost +")");
                } else {
                  console.log("Usuario creado correctamente");

                  // Automatic login for created user
                  // simple security token with 1h expiration.
                  var token = jwt.sign({ email: postBody.email, password: postBody.password, expire: Math.floor(Date.now() / 1000) + (60 * 60)}, jwtTokenKey);
                  // console.log(token);

                  response = {
                    "msg" : "Creación correcta del usuario",
                    "userid" : newUserId,
                    "token" : token
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Error en la creación de nuevo usuario"
            }
            console.log("Error en la creación de nuevo usuario");
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
);

/********************
*** Update a given user to the DB.
*** Updates the user with the information given as part of the JSON payload
*********************/
app.put ("/apitechu/v3/users/:id",
  function(req, res) {
    console.log("PUT /apitechu/v3/users/:id");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        // console.log(req.body);
        var updateUserId = req.params.id;

        var getQuery = 'q={"id" : ' + updateUserId + '} ';

        var putBody = req.body;
        putBody.id = Number(updateUserId);

        var httpClient = requestJSON.createClient(baseMLabURL);

        // console.log("user?" + getQuery + "&"+ mLabAPIKey);
        httpClient.put("user?" + getQuery + "&"+ mLabAPIKey, putBody,
          function(errPut, resMLabPut, bodyPut) {
            if (errPut) {
              response = {
                "msg" : "Error durante la actualización de un usuario"
              }
              res.status(500);
              console.log("Error durante la actualización de un usuario("+ errPut +")");
            } else {
              console.log("Usuario actualizado correctamente");
              response = {
                "msg" : "Actualización correcta del usuario",
                "userData" : putBody
              }
            }
            res.send(response);
          }
        )
      }
    )
  }
);

/********************
*** Basic login for given email and password.
*** Arguments passed as part of the payload.
*********************/
app.post("/apitechu/v3/login",
  function(req, res) {
    console.log("POST /apitechu/v3/login");

    var email = req.body.email;
    var password = req.body.password;

    // simple security token with 1h expiration.
    var token = jwt.sign({ email: req.body.email, password: req.body.password, expire: Math.floor(Date.now() / 1000) + (60 * 60)}, jwtTokenKey);
    // console.log(token);

    var query = 'q={"email":"' + email + '", "password":"' + password + '"}';

    var putBody = '{"$set" : {"logged" : "true"}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);


    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {


          if (bodyGet.length > 0) {

            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGIN"
                  }
                  res.status(500);
                  console.log("Error durante el LOGIN("+ errPut +")");
                } else {
                  console.log("Login ok");
                  response = {
                    "msg" : "CORRECT LOGIN for user",
                    "userid" : bodyGet[0]["id"],
                    "token"  : token
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Usuario o contraseña incorrectos"
            }
            console.log("Failed LOGIN");
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)

/********************
*** Logs out the given user given as a user id.
*** id is passed as part of the JSON payload.
*********************/
app.post("/apitechu/v3/logout",
  function(req, res) {
    console.log("POST /apitechu/v3/logout");

    var id = req.body.id;

    var query = 'q={"id":' + id + '}';

    var putBody = '{"$unset" : {"logged" : ""}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);


    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {

          if (bodyGet.length > 0) {
            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGOUT"
                  }
                  res.status(500);
                  console.log("Error durante el LOGOUT("+ errPut +")");
                } else {
                  console.log("LOGOUT ok");
                  response = {
                    "msg" : "CORRECT LOGOUT for user"
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Logger user not found"
            }
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)

////
// ACCOUNTS
////
/********************
*** Get all accounts for a given user (id).
*** Returns an array of account objects for the given user id.
*********************/
app.get ("/apitechu/v3/users/:id/accounts",
  function(req, res) {
    console.log("GET /apitechu/v3/users/:id/accounts");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        var id = req.params.id;
        var query = 'q={"userid":' + id + '}';

        var httpClient = requestJSON.createClient(baseMLabURL);

        httpClient.get("account?" + query + "&"+ mLabAPIKey,
          function(err, resMLab, body) {

            var response = {};

            if (err) {
              response = {
                "msg" : "Error obteniendo account"
              }
              res.status(500);
              console.log("Error obteniendo account. code("+ err +")");
            } else {
              if (body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "El usuario no tiene cuentas"
                }
                res.status(404);
              }
            }

            res. send(response);
          }
        )
      }
    )
  }
);

/***
 ** Delete an account for the given objectId _id
***/
app.delete ("/apitechu/v3/accounts/:_id",
  function(req, res) {
    console.log("DELTE /apitechu/v3/accounts/:_id");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        var objectId = req.params._id;

        var httpClient = requestJSON.createClient(baseMLabURL);

        httpClient.delete("account/" + objectId + "?"+ mLabAPIKey,
          function(err, resMLab, bodyDelete) {

            var response = {};

            if (err) {
              response = {
                "msg" : "Error deleting account"
              }
              res.status(500);
              console.log("Error deleting account. code("+ err +")");
            } else {
              response = bodyDelete;
            }

            res. send(response);
          }
        )
      }
    )
  }
);


//
/********************
*** Add a new account to the DB.
*** Adds an account with the given inforation provided as part of the JSON payload.
*********************/
app.post ("/apitechu/v3/accounts",
  function(req, res) {
    console.log("POST /apitechu/v3/accounts");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {
        // Query DB to obtain the next valid id.
        var userid = req.body.userid;
        var postBody = {"userid" : req.body.userid};


        var getQuery = 's={"id" : -1} ';

        console.log("No Cliente HTTP lanzado");
        var httpClient = requestJSON.createClient(baseMLabURL);


        httpClient.get("account?" + getQuery + "&"+ mLabAPIKey,
          function(errGet, resMLab, bodyGet) {

            if (errGet) {
              response = {
                "msg" : "Error en la creación de nueva cuenta"
              }
              res.status(500);
              res.send(response);
              console.log("Error obteniendo cuenta. code("+ errGet +")");
            } else {

              if (bodyGet.length > 0) {
                // Create new account inforation
                var newAccountId =
                postBody.id = bodyGet[0].id + 1; //new account id
                postBody.IBAN = faker.finance.iban();
                postBody.balance = 0;

                httpClient.post("account?" + mLabAPIKey, postBody,
                  function(errPost, resMLabPost, bodyPost) {
                    if (errPost) {
                      response = {
                        "msg" : "Error durante la creación de nueva cuenta"
                      }
                      res.status(500);
                      console.log("Error durante la creación de nueva cuenta("+ errPost +")");
                    } else {
                      console.log("Cuenta creada correctamente");
                      response = {
                        "msg" : "Creación correcta de la cuenta",
                        "account" : bodyPost
                      }
                    }
                    res.send(response);
                  }
                )
              } else {
                response = {
                  "msg" : "Error en la creación de nueva cuenta"
                }
                console.log("Error en la creación de nueva cuenta");
                res.status(404);
                res.send(response);
              }
            }
          }
        )
      }
    )
  }
);

/*********
* update balance of the account adding or substracting the given amount
**********/
//
app.put ("/apitechu/v3/accounts/:id/balance",
  function(req, res) {
    console.log("POST /apitechu/v3/accounts/:id/balance");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        var balanceVariation = req.body.balance;
        if ((balanceVariation == null) || (balanceVariation == 0)) {
          response = {
            "msg" : "Cambios no necesarios. Balance 0 o null."
          }
          res.status(500);
          res.send(response);
          console.log("Cambios no necesarios. Balance 0 o null.. code("+ errGet +")");
        }
        var accountid = req.params.id;
        var getQuery = 'q={"id":' + accountid + '}';

        console.log("No Cliente HTTP lanzado");
        var httpClient = requestJSON.createClient(baseMLabURL);
        console.log("account?" + getQuery + "&"+ mLabAPIKey);
        httpClient.get("account?" + getQuery + "&"+ mLabAPIKey,
          function(errGet, resMLab, bodyGet) {

            if (errGet) {
              response = {
                "msg" : "Error al actualizar cuenta"
              }
              res.status(500);
              res.send(response);
              console.log("Error al actualizar cuenta. code("+ errGet +")");
            } else {
              console.log(bodyGet);
              //console.log(JSON.stringify(bodyGet));
              if (bodyGet.length > 0) {
                // Create new account inforation
                var newBalance = parseFloat(bodyGet[0].balance) + parseFloat(balanceVariation);
                var putBody = {"$set" : {"balance" : Number(newBalance) }};
                httpClient.put("account?" + getQuery + "&"+ mLabAPIKey, putBody,
                  function(errPut, resMLabPut, bodyPut) {
                    if (errPut) {
                      response = {
                        "msg" : "Error durante la actualización del balance de la cuenta"
                      }
                      res.status(500);
                      console.log("Error durante la actualización del balance de la cuenta("+ errPut +")");
                    } else {
                      console.log("Balance actualizado correctamente");
                      response = {
                        "msg" : "Balance actualizado correctamente",
                        "account" : bodyPut
                      }
                    }
                    res.send(response);
                  }
                )
              } else {
                response = {
                  "msg" : "Cuenta a actualizar no encontrada"
                }
                console.log("Cuenta a actualizar no encontrada");
                res.status(404);
                res.send(response);
              }
            }
          }
        )
      }
    )
  }
);


////////////////
// MOVEMENTS
////////////////

/****
** Get all movements for a given account id
** Returns an array of movements objects as part of the return JSON payload.
*****/
app.get ("/apitechu/v3/accounts/:id/movements",
  function(req, res) {
    console.log("GET apitechu/v3/accounts/:id/movements");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        var id = req.params.id;
        var query = 'q={"accountid":' + id + '}';

        var httpClient = requestJSON.createClient(baseMLabURL);

        httpClient.get("account_movements?" + query + "&"+ mLabAPIKey,
          function(err, resMLab, body) {

            var response = {};

            if (err) {
              response = {
                "msg" : "Error obteniendo account movements"
              }
              res.status(500);
              console.log("Error obteniendo account movements. code("+ err +")");
            } else {
              if (body.length > 0) {

                response = body;
              } else {
                response = {
                  "msg" : "La cuenta no tiene movimientos"
                }
                res.status(404);
              }
            }

            res. send(response);
          }
        )
      }
    )
  }
);


/****
** Add a new movement to the DB
** Returns the created movement if all went ok.
*****/
app.post ("/apitechu/v3/movements",
  function(req, res) {
    console.log("POST /apitechu/v3/movements");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        var getQuery = 's={"id" : -1} ';

        var postBody = req.body;

        var httpClient = requestJSON.createClient(baseMLabURL);

        httpClient.post("account_movements?" + mLabAPIKey, postBody,
          function(errPost, resMLabPost, bodyPost) {
            if (errPost) {
              response = {
                "msg" : "Error durante la creación de nuevo movimiento"
              }
              res.status(500);
              console.log("Error durante la creación de nuevo movimiento("+ errPost +")");
            } else {
              console.log("movimiento creado correctamente");
              response = {
                "msg" : "Creación correcta del movimiento",
                "movement" : bodyPost
              }
              var putBody = {"balance" : Number(bodyPost.importe)};

              // Update associated account balance
              var localAPIhttpClient = requestJSON.createClient(baseApiTechuURL);
              var token = req.headers.noltoken;
              //Add token to header for local call.
              localAPIhttpClient.headers['noltoken'] = token;
              localAPIhttpClient.put("accounts/"+ bodyPost.accountid +"/balance" , putBody,
                function(errPut, resPut, bodyPut) {
                  if (errPut) {
                    console.log("Error durante la actulización del balance asociada al movimiento ("+ errPut +")");
                  } else {
                    console.log("Balance de la cuenta asociada al movimiento actualizada correctamente");
                  }
                  res.send(response);
                }
              );
            }
          }
        )
      }
    )
  }
);

/***
 ** Delete movement for given object id  _id.
***/
app.delete ("/apitechu/v3/movements/:_id",
  function(req, res) {
    console.log("DELTE /apitechu/v3/movements/:_id");
    commonManageJWTHeaderCallback(req, res,
      function(req, res) {

        var objectId = req.params._id;

        var httpClient = requestJSON.createClient(baseMLabURL);

        httpClient.delete("account_movements/" + objectId + "?"+ mLabAPIKey,
          function(err, resMLab, bodyDelete) {

            var response = {};
            if (err) {
              response = {
                "msg" : "Error deleting movement"
              }
              res.status(500);
              console.log("Error deleting movement. code("+ err +")");
            } else {
              response = bodyDelete;
            }
            console.log("Movement deleted correctly");
            res. send(response);
          }
        )
      }
    )
  }
);

/****
** Common JWT processing method used by most API methods.
** If token is correct continues processing given callback function that should be the real API implmentation.
*****/
function commonManageJWTHeaderCallback(req, res, callback) {

    var token = req.headers.noltoken;

    if (!token) {
      console.log("No token provided");
      var errorResponse = {
        "msg" : "No token provided",
      }
      res.status(401);
      res.send(errorResponse);
      return;
    }


    // verify a token symmetric - synchronous
    var decoded = jwt.verify(token, jwtTokenKey, function(err, decoded) {
      // err
      if (err != null) {
        // errror in token verification
        console.log("error in token verification");
        console.log(JSON.stringify(err));
        var errorResponse = {
          "msg" : "Secutiry token validation falied, try to login again.",
          "err" : err,
        }
        res.status(401);
        res.send(errorResponse);
        return;
      }
      else {
        //Improvements no time to implement.
        // - Check expiration of token
        // - Check if calling user coincides with token and requested information.
        // For the moment if a valid token is recieved all is OK. Minimum security.

        // token verified corectly
        // console.log(JSON.stringify(decoded));
        callback(req, res);
      }
    }
  );
}
