FROM node

WORKDIR /apitechu

#Añadir codigo al directorio de trabajo
ADD . /apitechu

# Exponer el puerto 3000 que usamos para apitechu
EXPOSE 3000

# Ejecutar el comando npm start en el directorio apitechu
CMD ["npm", "start"]
