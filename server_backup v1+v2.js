//imports y arranque de framework express en puerto
var express = require('express');
//app framework variable
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL =  "https://api.mlab.com/api/1/databases/apitechunol/collections/";
var mLabAPIKey = "apiKey=cUz5TOj1kVHCy8vnDlIs46nphaBBJDC7";
var requestJSON = require('request-json');

// listen at defined port
app.listen(port);
console.log("API molona Escuchando en el puerto " + port);

app.get("/apitechu/v1",
// http request y response, los monta el  framework
  function(req, res) {
    console.log("GET /apitechu/v1");
    // Si se pone sin comilla simple el retorno lo interpreta directamente como un json
    //res.send('{"msg" : "Hola desde el puerto 3000 de APITechU"}');
    res.send({"msg" : "Hola desde el puerto 3000 de APITechU"});
  }

);

app.get ("/apitechu/v1/users",
  function(req, res) {
    console.log("GET /apitechu/v1/users");

    //res.sendFile('./usuarios.json'); DEPRECATED

    //opcion 1
    //res.sendFile('usuarios.json', {root: __dirname}); // __dirname variable del directorio actual de ejecución del script.

    //opcion 2
    var users = require('./usuarios.json');
    res.send(users);
  }
);


app.get ("/apitechu/v2/users",
  function(req, res) {
    console.log("GET /apitechu/v2/users");

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        res. send(response);
      }
    )
  }
);

app.get ("/apitechu/v2/users/:id",
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");
    //console.log("user?" + query + "&"+ mLabAPIKey);
    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(err, resMLab, body) {

        var response = {};
        console.log("error = " + err);
        if (err) {
          response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
          console.log("Error obteniendo usuario. code("+ err +")");
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }

        res. send(response);
      }
    )
  }
);

app.post("/apitechu/v1/users",
  function(req, res) {
    console.log("POST /apitechu/v1/users");

    //console.log(req.headers);
    console.log("first_name is" + req.body.first_name);
    console.log("last_name is" + req.body.last_name);
    console.log("country is" + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    }

//read file as JS object array and add a newUSer
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);

    var msg = "Usuario guardado correctamente"
    console.log(msg)
    res.send({"msg" : msg});
  }
);

app.delete("/apitechu/v1/users/:id",
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);

    var msg = "Usuario borrado"
    console.log(msg)
    res.send({"msg" : msg});

  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");
    console.log("Parametros");
    console.log(req.params);
    console.log(req.params.p1);
    console.log(req.params.p2);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("headers");
    console.log(req.headers);

  }
)

///////////////////////////////////
/// PRACTICA 1 API REST Login -logout
///////////////////////////////////


app.post("/apitechu/v1/login",
  function(req, res) {
    console.log("POST /apitechu/v1/login");

    var email = req.body.email;
    var password = req.body.password;

    //read file as JS object array and add a newUSer
    var users = require('./usuarios.json');

    var loogedUser = null;
    for (user of users) {
      //console.log("email " + user.email);
      if (user.email == email && user.password == password) {
          //console.log("found password " + user.password);
          user.logged = true;
          loogedUser = user;
      }
    }

    if (loogedUser != null) {
      writeUserDataToFile(users);

      var msg = "LOGIN CORRECT: User " + loogedUser.email + " logged correctly"
      console.log(msg)
      res.send({"msg" : msg, "id" : loogedUser.id, "email" : loogedUser.email});
    }
    else {
      var msg = "LOGIN FAILED: User "+ email + " no logged"
      console.log(msg)
      res.send({"msg" : msg});
    }
  }
)

app.post("/apitechu/v2/login",
  function(req, res) {
    console.log("POST /apitechu/v2/login");

    var email = req.body.email;
    var password = req.body.password;

    console.log(req.body);

    var query = 'q={"email":"' + email + '", "password":"' + password + '"}';

    var putBody = '{"$set" : {"logged" : "true"}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {


          // console.log("bodyGet = " + JSON.stringify(bodyGet));
          //
          // var loggedUser = JSON.stringify(bodyGet);
          //
          // console.log("loggedUser.logged = " + loggedUser.logged);
          // console.log("type" + typeof(loggedUser.logged));
          //
          // if (loggedUser.logged == true) {
          //   console.log("if (loggedUser.logged) == true");
          // }
          // else {
          //   console.log("if (loggedUser.logged) == false");
          // }

          if (bodyGet.length > 0) {
            //console.log("user?" + query + "&"+ mLabAPIKey + "_______" + JSON.parse(putBody));
            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGIN"
                  }
                  res.status(500);
                  console.log("Error durante el LOGIN("+ errPut +")");
                } else {
                  console.log("Log ok");
                  response = {
                    "msg" : "CORRECT LOGIN for user",
                    "userid" : bodyGet[0]["id"]
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Usuario o contraseña incorrectos"
            }
            console.log("Failed LOGIN");
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)


app.post("/apitechu/v1/logout",
  function(req, res) {
    console.log("POST /apitechu/v1/logout");

    var id = req.body.id;

    //read file as JS object array and add a newUSer
    var users = require('./usuarios.json');

    var foundUser = null;
    var userNotLogged = false;
    for (user of users) {
      if (user.id == id) {
        console.log("foudn user id " + id);
        foundUser = user;
          if (user.logged == true) {
            console.log("user loogged id " + id);
            delete user.logged;

          }
          else {
            console.log("user not logged id " + id);
            userNotLogged = true;
          }
      }
    }

    if (foundUser != null) {
      if (userNotLogged == true) {
        // Tried to log out a non logged user
        var msg = "LOGOUT FAILED: User " + foundUser.email + "(" + foundUser.id+ ")" +  " found but not previously logged in."
        console.log(msg)
        res.send({"msg" : msg, "id" : foundUser.id, "email" : foundUser.email});
      }
      else {
        // User previously logged and logged out correctly.
        writeUserDataToFile(users);

        var msg = "LOGOUT CORRECT: User " + foundUser.email + "(" + foundUser.id+ ")" +  " logged out correctamente"
        console.log(msg)
        res.send({"msg" : msg, "id" : foundUser.id, "email" : foundUser.email});
      }
    }
    else {
      var msg = "LOGOUT FAILED: User"+ id + " not found."
      console.log(msg)
      res.send({"msg" : msg});
    }
  }
)


app.post("/apitechu/v2/logout",
  function(req, res) {
    console.log("POST /apitechu/v2/logout");


    var id = req.body.id;

    var query = 'q={"id":' + id + '}';

    var putBody = '{"$unset" : {"logged" : ""}}';
    var response = {};

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");

    console.log("user?" + query + "&"+ mLabAPIKey);
    httpClient.get("user?" + query + "&"+ mLabAPIKey,
      function(errGet, resMLab, bodyGet) {

        console.log("error = " + errGet);
        if (errGet) {
          response = {
            "msg" : "Error durante el LOGIN"
          }
          res.status(500);
          res.send(response);
          console.log("Error obteniendo usuario. code("+ errGet +")");
        } else {

          console.log("bodyGet = " + JSON.stringify(bodyGet));
          if (bodyGet.length > 0) {
            console.log("user?" + query + "&"+ mLabAPIKey + "_______" + JSON.parse(putBody));
            httpClient.put("user?" + query + "&"+ mLabAPIKey, JSON.parse(putBody),
              function(errPut, resMLabPut, bodyPut) {
                if (errPut) {
                  response = {
                    "msg" : "Error durante el LOGOUT"
                  }
                  res.status(500);
                  console.log("Error durante el LOGOUT("+ errPut +")");
                } else {
                  console.log("LOGOUT ok");
                  response = {
                    "msg" : "CORRECT LOGOUT for user"
                  }
                }
                res.send(response);
              }
            )
          } else {
            response = {
              "msg" : "Failed LOGIN: Logger user not found"
            }
            res.status(404);
            res.send(response);
          }
        }
      }
    )
  }
)

////
// ACCOUNTS
////

app.get ("/apitechu/v2/users/:id/accounts",
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid":' + id + '}';

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP lanzado");
    console.log("account?" + query + "&"+ mLabAPIKey);
    httpClient.get("account?" + query + "&"+ mLabAPIKey,
      function(err, resMLab, body) {

        var response = {};
        console.log("error = " + err);
        if (err) {
          response = {
            "msg" : "Error obteniendo account"
          }
          res.status(500);
          console.log("Error obteniendo account. code("+ err +")");
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "El usuario no tiene cuentas"
            }
            res.status(404);
          }
        }

        res. send(response);
      }
    )
  }
);


//REfactor
function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonData, "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Datos escritos en el fichero");
      }
    }
  );
}
